  
var common = require('./common.js').Common;
var Twitter = require('./TwitterScraper.js');

exports.Twitter = (function(){

  return {
    getImagesByUser       : function(req, res) {
      ///:user/:minPosition/:limitList

      var tUser = req.params.user; 

      var minPosition = req.params.minPosition; 
      console.dir(req.params)
     // var maxPosition= req.params.maxPosition; 
      var tLimitList = req.params.limitList; 

      if(common.isValidParam(tUser)){

        var t = new Twitter.Twitter(tUser);

        // if(common.isValidParam(maxPosition)){
        //   t.config.maxPosition= maxPosition;  
        // } 

        if(common.isValidParam(minPosition)){
          t.config.minPosition= minPosition;  
        }
        if(common.isValidParam(tLimitList)){
          t.config.limitList = tLimitList;  
        }
        t.config.limitList = 10;
        try{
          t.scraperImages(function(error, data){ 
            console.log('-------------- scraperImages - Out: ')
            //console.dir(data);
            console.log('------------------------------------- ')
            res.json(data);
          });
        }catch(ex){

            console.log(ex.code);
            res.json('Error!!');
      
        }

      }else{
        //usuario invalido
      }
      
    },
  }

})();

