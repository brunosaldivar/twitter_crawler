/*API, funcion que cdo le pase el usuario obtenga las X primeras fotos
pido las primeras 10, comparo moreItems con el lenght del array y con la cantidad q pedi, si me pase guardo el ultimo id y
solo doy las q pedi?

*/


var BigNumber = require('bignumber.js');//.BigNumber;
var cheerio = require('cheerio');
var request = require('request');
var iconv  = require('iconv').Iconv;
var fs = require('fs');

var Twitter = function(tUser){
	var oData = {
            imgList : [],
            hasMoreItems : false,
            error : {
                  code : null,
                  msg : '',
            },
      };

    //padre data-tweet-id Y data-name
      //class=content //todo con la cab y el det 
      //class="stream-item-header" cabecera
      //    img class=avatar js-action-profile-avatar
      //    a "tweet-timestamp js-permalink js-nav js-tooltip" title=
      //p "TweetTextSize TweetTextSize--16px js-tweet-text tweet-text" texto
      //js-media-img-placeholder attr data-img-src

	this.config = {
		baseURL                :	'https://twitter.com/i/profiles/show/twitterUser/media_timeline',
		twitterUser            : 	tUser,//'nanibenavente', Danacienfuegos
		params                 : 	'', 
		img                    :     '.js-media-img-placeholder', //'.TwitterPhoto-mediaSource',
		pClass 	           :     '.tweet-text', //'.ProfileTweet-text',
		aDate 	           :     '.tweet-timestamp', //'.ProfileTweet-timestamp',
    dvContent              :     '.content',
    dvTweet                :     '.content',
    dvTweetHeader          :     '.stream-item-header',
    pText                  :     '.tweet-text',
    limitList              :     0,
    lastId                 :        '',
    minPosition            :    0,
    maxPosition            :    0,
	};

	this.evalImages =  function (response, html, callback) {

    // try{
     console.log('statusCode: ' + response.statusCode);
    if(response.statusCode == 200) {

          var ctx = this;
          var responseJSON = JSON.parse(response.body);
          var ic = new iconv('iso-8859-1', 'utf-8');                              
          var htm = ic.convert(new Buffer(responseJSON.items_html,'binary')).toString('utf-8');                                                   
          //var htm = buf.toString('utf-8');  
          $ = cheerio.load(htm);
          var dvContents = $(ctx.config.dvContent);

          var minPosition = 0;
          var maxPosition = 0;

          dvContents.each( function(i, dvt) {

            var dvHeader = $(this).find(ctx.config.dvTweetHeader); 
            if(dvHeader == null) {console.log( ctx.config.dvTweetHeader + ' not found!'); return false;}

            var avatar = dvHeader.find('.avatar').attr('src');
            if(avatar == null) {console.log('avatar not found!'); return false;}

            //cambió 12/11/2015 aprox
            //var src = $(this).find(ctx.config.img).data('img-src'); 
            ////////
            //console.log($(this).html());
            var src = $(this).find('.AdaptiveMedia-photoContainer img').attr('src');
              if(src == null) {console.log('src not found!'); //return false;
            }                

            var tw = {
                id: $(this).parent('.tweet').data('item-id'),
            };
            //ver que hacemos con los videos
            if(src){
                //src = src + '';
                var fecha = dvHeader.find(ctx.config.aDate).attr('title');
                if(fecha == null) {console.log('fecha not found!'); return false;}            

                var texto = $(this).find(ctx.config.pText).text();

                tw.src = src;
                tw.text = texto.replace('\n', '').trim();
                tw.date = fecha;
                
                oData.imgList.push(tw);
                oData.username = $(this).parent().data('name');
                oData.avatar = avatar;
                //console.log('src:' + src +' id: ' + tw.id + ' min:' + minPosition + ' max:' + maxPosition);
            }
            maxPosition = tw.id;    
            minPosition = (minPosition === 0 ? tw.id :minPosition);
            
	        });
          
          oData.hasMoreItems = (oData.imgList.length == 1 ? false: true); //  responseJSON.has_more_items;
          oData.maxPosition = maxPosition;

          oData.minPosition = (oData.minPosition === undefined ?  minPosition : oData.minPosition);
         // ctx.config.params = minPosition;

          if(responseJSON.has_more_items){
                //oData.hasMoreItems = true;

                 if(oData.imgList.length >= ctx.config.limitList ){ 
                       callback(null,oData);
                 }else{
                      //console.log('maxPosition: ' + maxPosition);            
                      //'min-position' lastID
                      ctx.config.params =  '?include_available_features=1&include_entities=1&max_position=' + minPosition; //ctx.setParams();
                      //probar si la primera vez pide el +1 y luego siempre el max_id, aca siempre pongo +1
                       //ctxonsole.dir(ctx.config);
                      // console.log('responseJSON.max_id: ' + responseJSON.max_id);

                      // var icq = new iconv('iso-8859-1', 'utf-8');                              
                      // var htmq = icq.convert(new Buffer(responseJSON.items_html,'binary')).toString('utf-8');   
                      // lo = cheerio.load(htm);
                      // fs.writeFile('/home/bruno/Informacion/html',lo('li'), function (err,data) {
                      //   if (err) {
                      //     return console.log(err);
                      //   }
                      //   //console.log(data);
                      // });



                      //console.dir(responseJSON);
                      ctx.scraperImages(callback);
                     // callback(null,oData);
                }
          }else{
             // oData.hasMoreItems = true;
              callback(null,oData);
          }     
    }
    else{
          if(response.statusCode  == 404){
              oData.error.code = 404;
              oData.error.msg = 'User not found!';
          }
          callback(oData.error,oData);
    }

	}
	this.scraperImages = function(callback){

  	var url = this.config.baseURL.replace('twitterUser', this.config.twitterUser);
    // console.log(this.config);
    //if(this.config.maxPosition != 0){ 
    if(this.config.minPosition !== '0' ){ 
          this.config.params = '?include_available_features=1&include_entities=1&max_position='+ this.config.minPosition;// this.setParams();
    }
   // }
           
    var ctx = this;
    console.log(this.config.minPosition);
    console.log(url + this.config.params);

    request.call(ctx,url + this.config.params, function(error,response, html){
      if (!error){
		    ctx.evalImages.call(ctx, response, html, callback)
      }else{
          console.log(error) 
          callback(error,null);
      }
    });
	};

  //pasar ultimo como min
      this.setParams = function(){ 
            var rtn = '';
            // if(max !== 0) {
            //   var bgLastId = new BigNumber(max.toString());
            //   rtn = '&max_position='+ bgLastId ;
            // } 
           // var contextual_tweet_id = bgLastId.plus(1).toString();     
           // return '?contextual_tweet_id=' + contextual_tweet_id  + '&include_available_features=1&include_entities=1&max_id='+ contextual_tweet_id; 
          //veo q le manda el max nada mas en el postman

            var min = this.config.minPosition;
            //console.log('include_available_features=1&include_entities=1&max_position='+ max);
            return '?include_available_features=1&include_entities=1&max_position='+ min; 
            //return '?include_available_features=1&include_entities=1' + rtn;
      };
}


exports.Twitter = Twitter;