
App.Controllers.Twitter = function( $cookies, $q, $rootScope, $scope, TwitterService, LxProgressService, LxNotificationService,LxDialogService)
{

	

	$scope.config = {};
	$scope.data = {
		MediaTweets : [],
	};
	$scope.NoMore = false;
	$scope.config.user = 'infobae';
	$scope.config.csrftoken =  $cookies.get('_csrf');
	$scope.config.limitList = '20';
	$scope.config.minPosition = '0';
	$scope.config.maxPosition = '0';

	$scope.Math = window.Math;

	$scope.TweeterIds = [];
	$scope.at = [];

	$scope.scroll = {
		busy : true,
		hasMoreItems : true,
		loadMore : null,
	}

	$scope.ImageSelected = null;

	$scope.imgOpenImage = function(elem, tweet){


		$scope.ImageSelected = tweet;
		
		// var image = new Image();

  //       image.onabort = image.error = function(){
  //       	image.src = ""; //poner img de error
  //       }
  //       image.onload = function() {
  //       	image.count = ct;
  //       	$scope.count++;
  //           resolve(image);
  //       }
  //       image.src = src;

		LxDialogService.open('test');
	}


	$scope.counting = function(callback){
		if(!$scope.NoMore ){
			LxNotificationService.info( $scope.data.MediaTweets.length + ($scope.data.MediaTweets.length == 0 ? ' images' : ' images...'));
		}else{
			LxNotificationService.info($scope.data.MediaTweets.length + ' images');
			LxNotificationService.error('No more Items');
		}
	}

	$scope.scroll.loadMore = function(){
		
		if($scope.data.MediaTweets.length != 0){
			if(!$scope.scroll.busy && $scope.scroll.hasMoreItems ){

				LxProgressService.linear.show('#5fa2db', '#progress');

				$scope.scroll.busy = true;

				$scope.config.lastId = $scope.TweeterIds[$scope.TweeterIds.length -1];
				//console.log($scope.config.lastId);
				$scope.getImages(
					function(){
						$scope.scroll.busy = false;
						LxProgressService.linear.hide();
						$scope.counting();
					});
			}
		}
	}
	
	$scope.createImage = function(src,ct) {

	    return $q(function(resolve, reject) {

	        var image = new Image();

	        image.onabort = image.error = function(){
	        	image.src = ""; //poner img de error
	        }
	        image.onload = function() {
	        	image.count = ct;
	        	$scope.count++;
	            resolve(image);
	        }
	        image.src = src;

	    });
	}
	$scope.btnGetMediaTweet = function(){
		console.log($scope.csrftoken)
		if($scope.config.user == ''){
			LxNotificationService.error('User incorrect');
			return false;
		}
		if($scope.config.user.indexOf('@') == -1) {
			$scope.config.user = $scope.config.user.replace('@','');
		}
		///reset
		$scope.data.MediaTweets = [];
		$scope.TweeterIds  = [];
		$scope.config.lastId = '0';
		$scope.config.minPosition = '0';
		$scope.config.maxPosition = '0';
		$scope.NoMore = false;
		///

		LxProgressService.linear.show('#5fa2db', '#progress');
		$scope.getImages(
					function(){

						$scope.scroll.busy = false;
						LxProgressService.linear.hide();
						$scope.counting();

					});
	}

	$scope.getImages = function(callback){

		if(!$scope.NoMore){

			TwitterService.getMediaTweets($scope.config, function(data){

		        $scope.scroll.hasMoreItems = data.hasMoreItems;
		        console.log('callback: getMediaTweets');
				$scope.config.minPosition = data.minPosition;
				$scope.config.maxPosition =  data.maxPosition;

				console.log($scope.config.maxPosition );

		        if(data.error.code == null){
					var flgMoreItems = true;
			        
			        if (data.imgList.length ==1 && !data.hasMoreItems) {
			        	if(data.imgList[0].id == $scope.config.lastId ){
			        		flgMoreItems = false;
			        		LxProgressService.linear.hide();
			        		console.log('no more items');
			        	}
			        }

			        if(flgMoreItems){
			        	var flgCount = 0;
			        	
						for (var i= 0; i < data.imgList.length; i++ ){

							var id = data.imgList[i].id.toString();
							if($scope.TweeterIds.indexOf(id) == -1){
								
								$scope.TweeterIds.push(id);
								$scope.createImage(data.imgList[i].src, i).then(function(imgObj){
									
									var w = imgObj.width / (imgObj.width > 700 ? 3 :  ( imgObj.width  <= 260 ? 1 :2 )  );
									var h = imgObj.height / (imgObj.height > 600 ?  3 : (imgObj.height <= 300 ? 1 : 2 ))  ;

						            data.imgList[imgObj.count].width =  w  + 'px';
					            	data.imgList[imgObj.count].height = h+ 'px';

					            	var txt = data.imgList[imgObj.count].text.trim();
					            	
					            	var pos = txt.indexOf('pic.twitter.com');
					            	if(pos != -1){
					            		txt = txt.replace('pic.twitter.com', ' http://pic.twitter.com');
					            	}
					            	data.imgList[imgObj.count].text = txt;
					             	$scope.data.MediaTweets.push(data.imgList[imgObj.count]);
								}); 
							}else{
								flgCount++;
				        	}
				        }

				        if(data.imgList.length === flgCount){
				        	$scope.NoMore = true;
				        	LxProgressService.linear.hide();
				        	console.log('no more items');
				        } //listo
				        //console.log(flgCount);
			    	}
		    	}else{

		    		LxNotificationService.error(data.error.msg);
		    	}

		        //console.log(data);
				if(callback)
					callback(data);
			});
		}
	}
}