
var App = {
    Data : {
        MediaTweets : [],
    },
    Common : {},
    Providers: {},
    Directives: {},
    Views: {},
    Services: {},
    Controllers: {},

    init: function () {  


        //var twitterApp = angular.module('twitterApp', ['lumx','wu.masonry']);
        var twitterApp = angular.module('twitterApp',['wu.masonry', 'ngCookies', 'ngSanitize', 'lumx', 'infinite-scroll']);

        twitterApp.factory('TwitterService', ['$http',  App.Services.Twitter]);
        //providers
        //linkslyApp.provider('$statisticProvider',App.Providers.Statistic);
        //////

        //https://gist.github.com/rishabhmhjn/7028079
        twitterApp.filter('tweetLinky',['$filter', 
                function($filter) {
                    return function(text, target) {
                      if (!text) return text;

                      var replacedText = $filter('linky')(text, target);
                      var targetAttr = "";
                      if (angular.isDefined(target)) {
                          targetAttr = ' target="' + target + '"';
                      }

                      // replace #hashtags
                      var replacePattern1 = /(^|\s)#(\w*[a-zA-Z_]+\w*)/gim;
                      replacedText = replacedText.replace(replacePattern1, '$1<a href="https://twitter.com/search?q=%23$2"' + targetAttr + '>#$2</a>');

                      // replace @mentions
                      var replacePattern2 = /(^|\s)\@(\w*[a-zA-Z_]+\w*)/gim;
                      replacedText = replacedText.replace(replacePattern2, '$1<a href="https://twitter.com/$2"' + targetAttr + '>@$2</a>');

                      //$sce.trustAsHtml(replacedText);
                      return replacedText;
                       };
                    }
        ]);

        twitterApp.controller('TwitterController', [ '$cookies', '$q','$rootScope', '$scope', 'TwitterService', 'LxProgressService', 'LxNotificationService', 'LxDialogService',App.Controllers.Twitter]);
       
        twitterApp.directive('twitterImage', App.Directives.Image);

    }
};