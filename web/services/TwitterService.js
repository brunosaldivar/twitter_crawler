App.Services.Twitter = function( $http)
{
	var twitterService = {};
	
	twitterService.data = {
            imgList : [],
            hasMoreItems : false,
            error : {
                  code : null,
                  msg : '',
            },
     };

	twitterService.getMediaTweets = function(config, callback){

		var limit = config.limitList;


		//$http.get('/getImages/' + config.user + '/' + config.minPosition + '/' + config.maxPosition + '/'+ config.limitList)
		//console.log('config.maxPosition:' + config.maxPosition);

		// var data = {
		// 	user : config.user, 
		// 	minPosition : config.minPosition,
		// 	maxPosition : config.maxPosition,
		// 	limitList : config.limitList,
		// 	csrftoken  : 0, // config.csrftoken,
		// };

		// $http.post('/getImages', data)
		// 	   .then(
		// 	       function(response){

		// 		        if(response.data !==""){
		// 					callback(response.data); 
		// 				}else{
		// 					callback(twitterService.data); 
		// 				}
		// 	       }, 
		// 	       function(err){
		// 	         	twitterService.data.error.code = 0;
		// 				twitterService.data.error.msg = 'Cannot connect to server';
		// 				callback(twitterService.data); 
		// 	       }
		// 	    );

		$http({ method  : 'GET', 
				url: '/api/getImages/' + config.user + '/' + config.maxPosition + '/'+ config.limitList
				// ,header: {
    //  					   'Authorization': $scope.authtoken
    // 					}
    		})
		.success(function(data, status, headers, config) {

			if(data!==""){
				callback(data); 
			}else{
				callback(twitterService.data); 
			}

		}).error(function (err){

			twitterService.data.error.code = 0;
			twitterService.data.error.msg = 'Cannot connect to server';
			callback(twitterService.data); 
		});

	}
	return 	twitterService;
}
